#define EXTRA_CREDIT
#ifdef EXTRA_CREDIT
#include "xmergesort.h"
#include "xmergesort_args.h"

#ifndef __NR_xmergesort
#error xmergesort system call not defined
#endif


int main(int argc, char *argv[])
{
	long int ret_val;
	int flags;
	int input_files = 1;
	xmergesort_args arg_struct;

	memset(&arg_struct, 0, sizeof(arg_struct));
	/*  Parse the command line for flags */
	while ((flags = getopt(argc, argv, "uaitd")) != -1) {
		switch (flags) {
		case 'u':
			arg_struct.flags |= 0x01;
			break;
		case 'a':
			arg_struct.flags |= 0x02;
			break;
		case 'i':
			arg_struct.flags |= 0x04;
			break;
		case 't':
			arg_struct.flags |= 0x10;
			break;
		case 'd':
			arg_struct.flags |= 0x20;
			arg_struct.data = malloc(1*sizeof(unsigned int));
			break;
		case '?':
			fprintf(stderr, "Unknown flag input: \"%c\".\n", optopt);
			goto commandline_error;
		}
	}
	/* Setting both -u and -a is error */
	if ((arg_struct.flags & 0x01) && (arg_struct.flags & 0x02)) {
		errno = EINVAL;
		perror("Both 'unique records' [-u] and 'all records' [-a] flags set, abort!\n");
		goto commandline_error;
	}
	/* Setting neither of -u and -a is also an error */
	if (!(arg_struct.flags & 0x01) && !(arg_struct.flags & 0x02)) {
		errno = EINVAL;
		perror("Both 'unique records' [-u] and 'all records' [-a] flags are absent, abort!\n");
		goto commandline_error;
	}
	/* Parse the command line for input/output files */
	switch (argc-optind) {
	case 0:
		errno = EINVAL;
		perror("No output/input files provided.\n");
		goto commandline_error;
	case 1:
		errno = EINVAL;
		perror("No input file provided.\n");
		goto commandline_error;
	case 2:
		errno = EINVAL;
		perror("Atleast two input files are required.\n");
		goto commandline_error;
	default:
		if ((argc - optind - 1) > 10) {
			errno = EINVAL;
			perror("A maximum of 10 input files can be supplied.\n");
			goto commandline_error;
		}
		printf("Output file : %s\n", argv[optind]);
		arg_struct.outfile = argv[optind];
		printf("Total input files: %d\n", argc - optind - 1);
		arg_struct.num_input_files = argc - optind - 1;
		printf("Input file(s):\n");
		for (input_files  = 1; input_files < argc-optind; input_files++) {
			arg_struct.infiles[input_files - 1] = argv[input_files + optind];
			printf("%s\n", argv[input_files + optind]);
		}
	}
	ret_val  = syscall(__NR_xmergesort, (void *)(&arg_struct));
	if (ret_val == 0)
		printf("syscall returned successfully!\n");
	/* Error messages to user */
	else {
		fprintf(stderr, "syscall returned with error: %d!\n", errno);
		/* For VFS STAT failure, ENOENT */
		if (errno == ENOENT)
			perror("One or more input files do not exist!\n");
		/* For  ENOMEM */
		else if (errno == ENOMEM)
			perror("Unable to allocate memory in kernel!\n");
		/* For EIO */
		else if (errno == EIO)
			perror("Read/Write operation to file failed!\n");
		/* For EBADF */
		else if (errno == EBADF)
			perror("Invalid input/output files: Input/Output file not regular or multiple files found same!\n");
		/* For EFAULT */
		else if (errno == EFAULT)
			perror("Bad/NULL address found in user argument structure!\n");
		/* For EINVAL, system call sets this only for -t flag fail */
		else if (errno == EINVAL)
			perror("Unsorted records found in input!!\n");
		/* In all other cases */
		else
			perror("Something went wrong during syscall operation, refer kernel logs for the failure!\n");
	}
	/* If -d flag is set, print the records written */
	if (arg_struct.flags & 0x20)
		printf("Number of records written to output file is: %d\n", *(arg_struct.data));
	exit(ret_val);

commandline_error:
	errno = EINVAL;
	perror("Aborting with failure, issue in command line arguments!\n");
	return -1;
}

/*
 * This is the non extra credit part.
 * In this case, exactly two input files are expected.
 */
#else

#include "xmergesort.h"
#include "xmergesort_args.h"

#ifndef __NR_xmergesort
#error xmergesort system call not defined
#endif


int main(int argc, char *argv[])
{
	long int ret_val;
	int flags;
	int input_files = 1;
	xmergesort_args arg_struct;

	memset(&arg_struct, 0, sizeof(arg_struct));
	/* Parse the command line for flags */
	while ((flags = getopt(argc, argv, "uaitd")) != -1) {
		switch (flags) {
		case 'u':
			arg_struct.flags |= 0x01;
			break;
		case 'a':
			arg_struct.flags |= 0x02;
			break;
		case 'i':
			arg_struct.flags |= 0x04;
			break;
		case 't':
			arg_struct.flags |= 0x10;
			break;
		case 'd':
			arg_struct.flags |= 0x20;
			arg_struct.data = malloc(1*sizeof(unsigned int));
			break;
		case '?':
			fprintf(stderr, "Unknown flag input: \"%c\".\n", optopt);
			goto commandline_error;
		}
	}
	/* Setting both -u and -a is error */
	if ((arg_struct.flags & 0x01) && (arg_struct.flags & 0x02)) {
		errno = EINVAL;
		perror("Both 'unique records' [-u] and 'all records' [-a] flags set, abort!\n");
		goto commandline_error;
	}
	/* Setting neither of -u and -a is also an error */
	if (!(arg_struct.flags & 0x01) && !(arg_struct.flags & 0x02)) {
		errno = EINVAL;
		perror("Both 'unique records' [-u] and 'all records' [-a] flags are absent, abort!\n");
		goto commandline_error;
	}
	/* Parse the command line for input/output files */
	switch (argc-optind) {
	case 0:
		errno = EINVAL;
		perror("No output/input files provided.\n");
		goto commandline_error;
	case 1:
		errno = EINVAL;
		perror("No input file provided.\n");
		goto commandline_error;
	case 2:
		errno = EINVAL;
		perror("Exactly two input files are required.\n");
		goto commandline_error;
	default:
		if ((argc - optind - 1) > 2) {
			errno = EINVAL;
			perror("The input expects exactly 2 input files!\n");
			goto commandline_error;
		}
		printf("Output file : %s\n", argv[optind]);
		arg_struct.outfile = argv[optind];
		printf("Total input files: %d\n", argc - optind - 1);
		arg_struct.num_input_files = argc - optind - 1;
		printf("Input file(s):\n");
		for (input_files  = 1; input_files < argc-optind; input_files++) {
			arg_struct.infiles[input_files - 1] = argv[input_files + optind];
			printf("%s\n", argv[input_files + optind]);
		}
	}
	ret_val  = syscall(__NR_xmergesort, (void *)(&arg_struct));
	if (ret_val == 0)
		printf("syscall returned successfully!\n");
	/* Error messages to user */
	else {
		fprintf(stderr, "syscall returned with error: %d!\n", errno);
		/* For VFS STAT failure, ENOENT */
		if (errno == ENOENT)
			perror("One or more input files do not exist!\n");
		/* For  ENOMEM */
		else if (errno == ENOMEM)
			perror("Unable to allocate memory in kernel!\n");
		/* For EIO */
		else if (errno == EIO)
			perror("Read/Write operation to file failed!\n");
		/* For EBADF */
		else if (errno == EBADF)
			perror("Invalid input/output files: Input/Output file not regular or multiple files found same!\n");
		/* For EFAULT */
		else if (errno == EFAULT)
			perror("Bad/NULL address found in user argument structure!\n");
		/* For EINVAL, system call sets this only for -t flag fail */
		else if (errno == EINVAL)
			perror("Unsorted records found in input!!\n");
		/* In all other cases */
		else
			perror("Something went wrong during syscall operation, refer kernel logs for the failure!\n");
	}
	/* If -d flag is set, print the records written */
	if (arg_struct.flags & 0x20)
		printf("Number of records written to output file is: %d\n", *(arg_struct.data));
	exit(ret_val);

commandline_error:
	errno = EINVAL;
	perror("Aborting with failure, issue in command line arguments!\n");
	return -1;
}

#endif

