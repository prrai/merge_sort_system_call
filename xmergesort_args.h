typedef struct xmergesort_args {
	char *outfile;
	char *infiles[10];
	unsigned int num_input_files;
	unsigned int flags;
	unsigned int *data;
} xmergesort_args;
