#include "sys_xmergesort.h"
#include "xmergesort_args.h"

asmlinkage extern long (*sysptr)(void *arg);

/*
 *Perform user access and null checks on all the arguments received from user space
 *@kernel_args:   Structure allocated in kernel space, to copy user args
 *@user_args:     Structure type casted from void, received from user space
*/
int init_params(xmergesort_args *kernel_args, xmergesort_args *user_args)
{
	int retval = 0, in_iter = 0, in_iter2 = 0, ret_op_stat = 0, ret_in_stat = 0;
	char *curr_ptr = NULL;
	dev_t *cache_ip_dev = NULL;
	u64 *cache_ip_ino = NULL;
	struct kstat outfile_stat, curr_infile_stat;

	/* Validate user args struct*/
	if (NULL != user_args->outfile) {
		if (!access_ok(VERIFY_READ, user_args->outfile, sizeof(user_args->outfile))) {
			printk(KERN_ALERT "Invalid pointer to the output file detected!");
			retval = -EFAULT;
			goto ret_init_params;
		}
	} else {
		printk(KERN_ALERT "Output file pointer can not be NULL!");
		retval = -EFAULT;
		goto ret_init_params;
	}
	/* Copy num_of_input parameters to kernel space since we need it immediately, remaining elements copied later after access checks */
	retval = copy_from_user(&kernel_args->num_input_files, &user_args->num_input_files, sizeof(unsigned int));
	if (0 != retval) {
		printk(KERN_ALERT "Copy of number of input files from user to kernel space failed");
		goto ret_init_params;
	}
	/* Validate output file name container */
	for (in_iter = 0; in_iter < kernel_args->num_input_files; in_iter++) {
		curr_ptr = user_args->infiles[in_iter];
		if (NULL != curr_ptr) {
			if (!access_ok(VERIFY_READ, curr_ptr, sizeof(curr_ptr))) {
				printk(KERN_ALERT "Invalid pointer to an input file detected!");
				retval = -EFAULT;
				goto ret_init_params;
			}
		} else {
			printk(KERN_ALERT "Pointer to an input file can not be NULL!");
			retval = -EFAULT;
			goto ret_init_params;
		}
	}
	/*
	* Check if output file is regular, don't throw error if stat fails with 'file not found'
	* We will create output file in that case
	*/
	ret_op_stat = vfs_stat(user_args->outfile, &outfile_stat);
	if (-2 != ret_op_stat) {
		if (0 != ret_op_stat) {
			printk(KERN_ALERT "Stat operation on the output file failed!");
			retval = ret_op_stat;
			goto ret_init_params;
		} else {
			if (!(S_ISREG(outfile_stat.mode))) {
				printk(KERN_ALERT "Output file is not a regular file!");
				retval = -EBADF;
				goto ret_init_params;
			}
		}
	} else {
		printk(KERN_ALERT "Output file doesn't already exist, will be created.");
	}
	/* We will cache the inode and dev to prevent multiple calls to vfs_stat to all input files, allocate containers*/
	cache_ip_dev = kmalloc(kernel_args->num_input_files*sizeof(dev_t), GFP_KERNEL);
	if (NULL == cache_ip_dev) {
		printk(KERN_ALERT "Unable to allocate memory to store input file kstat dev field!");
		retval = -ENOMEM;
		goto ret_init_params;
	}
	cache_ip_ino = kmalloc(kernel_args->num_input_files*sizeof(u64), GFP_KERNEL);
	if (NULL == cache_ip_ino) {
		printk(KERN_ALERT "Unable to allocate memory to store input file kstat inode field!");
		retval = -ENOMEM;
		goto clear_cache_ip_dev;
	}
	/*
	* Check if any of the input files is same as output file(only if it exists)
	*  In the same traversal, check if input files are regular
	*/
	for (in_iter = 0; in_iter < kernel_args->num_input_files; in_iter++) {
		curr_ptr = user_args->infiles[in_iter];
		ret_in_stat = vfs_stat(curr_ptr, &curr_infile_stat);
		if (ret_in_stat) {
			printk(KERN_ALERT "Stat operation to an input file failed!");
			retval = ret_in_stat;
			goto clear_cache_ip_ino;
		} else {
			if (!(S_ISREG(curr_infile_stat.mode))) {
				printk(KERN_ALERT "An input file was found to be not regular!");
				retval = -EBADF;
				goto clear_cache_ip_ino;
			}

			if (-2 != ret_op_stat) {
				if ((curr_infile_stat.dev == outfile_stat.dev) && curr_infile_stat.ino == outfile_stat.ino) {
					printk(KERN_ALERT "Output file found same as one of the input files!");
					retval = -EBADF;
					goto clear_cache_ip_ino;
				}
			}
			cache_ip_ino[in_iter] = curr_infile_stat.ino;
			cache_ip_dev[in_iter] = curr_infile_stat.dev;
		}
	}
	/* Use cached containers to check if any of the input files are same*/
	for (in_iter = 0; in_iter < kernel_args->num_input_files-1; in_iter++) {
		for (in_iter2 = in_iter+1; in_iter2 < kernel_args->num_input_files; in_iter2++) {
			if ((cache_ip_ino[in_iter] == cache_ip_ino[in_iter2]) && (cache_ip_dev[in_iter] == cache_ip_dev[in_iter2])) {
				printk(KERN_ALERT "Two files in the input list detected same!");
				retval = -EBADF;
				goto clear_cache_ip_ino;
			}
		}
	}

	/* Start copying user arguments to kernel space */

	retval = copy_from_user(&kernel_args->flags, &user_args->flags, sizeof(unsigned int));
	if (0 != retval) {
		printk(KERN_ALERT "Copy of flags from user to kernel space failed");
		goto ret_init_params;
	}
	if (0 != ((kernel_args->flags) & 0x20)) {
		if (NULL != user_args->data) {
			if (!access_ok(VERIFY_READ, user_args->data, sizeof(user_args->data))) {
				printk(KERN_ALERT "Invalid pointer to the data buffer detected!");
				retval = -EFAULT;
				goto ret_init_params;
			}
		} else {
			printk(KERN_ALERT "Data buffer pointer can not be NULL!");
			retval = -EFAULT;
			goto ret_init_params;
		}
	}

	/* Copy output file name */
	kernel_args->outfile = kmalloc(strlen_user(user_args->outfile), GFP_KERNEL);
	retval = strncpy_from_user(kernel_args->outfile, user_args->outfile, strlen_user(user_args->outfile));
	if (0 > retval) {
		printk(KERN_ALERT "Copy of out file from user to kernel space failed");
		goto clear_kernel_outfile;
	}
	retval = 0;

	/*Copy all the input file names */
	for (in_iter = 0; in_iter < user_args->num_input_files; in_iter++) {
		kernel_args->infiles[in_iter] = kmalloc(strlen_user(user_args->infiles[in_iter]), GFP_KERNEL);
		retval = strncpy_from_user(kernel_args->infiles[in_iter], user_args->infiles[in_iter], strlen_user(user_args->infiles[in_iter]));
		if (0 > retval) {
			printk(KERN_ALERT "Copy of input files from user to kernel space failed");
			goto clear_kernel_infiles;

		}
	}
	retval = 0;
	goto clear_cache_ip_ino;

clear_kernel_infiles:
	/* Free all the input files */
	for (in_iter = 0; in_iter < kernel_args->num_input_files; in_iter++) {
		if (NULL == kernel_args->infiles[in_iter]) {
			kfree(kernel_args->infiles[in_iter]);
			kernel_args->infiles[in_iter] = NULL;
		}
	}
clear_kernel_outfile:
	kfree(kernel_args->outfile);
	kernel_args->outfile = NULL;
clear_cache_ip_ino:
	kfree(cache_ip_ino);
	cache_ip_ino = NULL;
clear_cache_ip_dev:
	kfree(cache_ip_dev);
	cache_ip_dev = NULL;
ret_init_params:
	return retval;
}

/*
 * Function to open file pointer for input file
 */
struct file *open_infile(char *infile)
{
	struct file *file_ptr;
	file_ptr = filp_open(infile, O_RDONLY, 0);
	if (NULL == file_ptr || IS_ERR(file_ptr)) {
		printk(KERN_ALERT "Unable to open file in read mode!");
	}
	return file_ptr;
}

/*
 * Function to open file pointer for output file, based on mode.
 * A new file is created if one does not exist.
 */
struct file *open_outfile(char *outfile, umode_t mode)
{
	struct file *file_ptr;
	file_ptr = filp_open(outfile, O_WRONLY|O_CREAT, mode);
	if (NULL == file_ptr || IS_ERR(file_ptr)) {
		printk(KERN_ALERT "Unable to open file in write mode!");
	}
	return file_ptr;
}

/*
 * Read file function, reads from the given offset
 */
int read_file(struct file *file_ptr, void *buffer, int offset)
{
	mm_segment_t old_fs;
	int bytes = 0;
	file_ptr->f_pos -= offset;
	old_fs = get_fs();
	set_fs(KERNEL_DS);
	bytes = vfs_read(file_ptr, buffer, PAGE_SIZE, &file_ptr->f_pos);
	set_fs(old_fs);
	return bytes;
}

/*
 *Function to write buffer contents to file
 */
int write_file(struct file *file_ptr, void *buffer, int size)
{
	mm_segment_t old_fs;
	int bytes = 0;
	old_fs = get_fs();
	set_fs(KERNEL_DS);
	bytes = vfs_write(file_ptr, buffer, size, &file_ptr->f_pos);
	set_fs(old_fs);
	return bytes;
}

/*
 *Merge sort function, user args struct passed to write to user space data pointer location, if -d flag is given.
 */
int merge_sort(xmergesort_args *kernel_args, xmergesort_args *user_args, char *infile1, char *infile2, char *outfile, umode_t outfile_mode)
{
	int retval = 0, chars_read1 = 0, in1_p1 = 0, rec_ctr = 0, in1_p2 = 0, in2_p1 = 0, \
	in2_p2 = 0, i_left = 0, i_left1 = 0, i_right1 = 0, i_right = 0, len_curr_rec = 0,\
	chars_read2 = 0, off = 0, rem_space_in_out = 0, temp_out = 0, temp_it = 0, curr_vs_lastw = 0, \
	temp_it2 = 0, should_we_write1 = 0, should_we_write2 = 0, len_curr_rec2 = 0;
	struct file *infile1_ptr = NULL;
	struct file *infile2_ptr = NULL;
	struct file *outfile_ptr = NULL;
	unsigned int *ptr = NULL;
	char *infile1_buf = NULL;
	char *infile2_buf = NULL;
	char *outfile_buf = NULL;
	char *temp_buf = kmalloc(PAGE_SIZE, GFP_KERNEL);
	infile1_buf = kmalloc(PAGE_SIZE, GFP_KERNEL);

	/* Allocate 1 buffer each for input files, 1 temporary
	* buffer for flag checks, and 1 output file buffer
	*/
	if (NULL == infile1_buf) {
		retval = -ENOMEM;
		goto clean_infile1_buf;
	}
	infile2_buf = kmalloc(PAGE_SIZE, GFP_KERNEL);
	if (NULL == infile2_buf) {
		retval = -ENOMEM;
		goto clean_infile2_buf;
	}
	outfile_buf = kmalloc(PAGE_SIZE, GFP_KERNEL);
	if (NULL == outfile_buf) {
		retval = -ENOMEM;
		goto clean_outfile_buf;
	}

	/* Open file pointers to input files, output file and temporary file */
	infile1_ptr = open_infile(infile1);
	if (NULL == infile1_ptr) {
		retval = -ENOENT;
		goto clean_outfile_buf;
	}
	infile2_ptr = open_infile(infile2);
	if (NULL == infile2_ptr) {
		retval = -ENOENT;
		goto clean_outfile_buf;
	}

	memset(outfile_buf, 0x00, PAGE_SIZE);

	outfile_ptr = open_outfile(outfile, outfile_mode);
	if (NULL == outfile_ptr) {
		retval = -ENOENT;
		goto clean_outfile_buf;
	}

	/* Read a PAGE SIZE fo data from input file in each input buffer */
	memset(infile1_buf, 0x00, PAGE_SIZE);
	chars_read1 = read_file(infile1_ptr, infile1_buf, 0);
	if (chars_read1 < 0) {
		printk(KERN_ALERT "File read to buffer failed!");
		retval = -EIO;
		goto clean_outfile_buf;
	}
	memset(infile2_buf, 0x00, PAGE_SIZE);
	chars_read2 = read_file(infile2_ptr, infile2_buf, 0);
	if (chars_read2 < 0) {
		printk(KERN_ALERT "File read to buffer failed!");
		retval = -EIO;
		goto clean_outfile_buf;
	}
	/* Merging loop */
	while (!((chars_read1 == 0) && (chars_read2 == 0))) {
		/* Read another page since the current one is done
		* If the read results in 0, the file has reached its end
		*/
		if (in1_p2 == strlen(infile1_buf) && (0 != chars_read1)) {
			off = in1_p2 - in1_p1;
			memset(infile1_buf, 0x00, PAGE_SIZE);
			chars_read1 = read_file(infile1_ptr, infile1_buf, off);
			if (chars_read1 < 0) {
				printk(KERN_ALERT "File read to buffer failed!");
				retval = -EIO;
				goto clean_temp_buf;
			}
			 in1_p1 = 0, in1_p2 = 0;
		}

		/* Similarly read a page for file 2 */
		if (in2_p2 == strlen(infile2_buf) && (0 != chars_read2)) {
			off = in2_p2 - in2_p1;
			memset(infile2_buf, 0x00, PAGE_SIZE);
			chars_read2 = read_file(infile2_ptr, infile2_buf, off);
			if (chars_read2 < 0) {
				printk(KERN_ALERT "File read to buffer failed!");
				retval = -EIO;
				goto clean_temp_buf;
			}
			 in2_p1 = 0, in2_p2 = 0;
		}

		/* If file 1 has ended, process the remaining records in file 2 */
		if (chars_read1 == 0) {
			if (chars_read2 != 0) {
				len_curr_rec = 0;
				/* Get the remaining space available in out buffer */
				rem_space_in_out = PAGE_SIZE - strlen(outfile_buf);
				temp_it = in2_p1;
				/* Get the length of current record */
				while (infile2_buf[temp_it++] != '\n')
					len_curr_rec++;
				len_curr_rec++;
				curr_vs_lastw = -1;
				/* Temp buffer holds the last record that was written to file */
				/* Check if the current record should be written to file based on the content of temp buffer */
				if (strlen(temp_buf) > 0) {
					curr_vs_lastw = 0;
					i_left1 = 0, i_right1 = 0, temp_it = 0;
					temp_it2 = in2_p1;
					while (!((temp_buf[temp_it] == '\n') && (infile2_buf[temp_it2] == '\n'))) {
						/* Make comparisons case-insensitive if the flag is set */
						if ((kernel_args->flags & 0x04) && (97 <= infile2_buf[temp_it2]) && (infile2_buf[temp_it2] <= 123))
							i_left1 = 1;
						if ((kernel_args->flags & 0x04) && (97 <= temp_buf[temp_it]) && (temp_buf[temp_it] <= 123))
							i_right1 = 1;
						/*  If the current rec is smaller, set curr_vs_lastw = 1. */
						if ((((infile2_buf[temp_it2] - (32 * i_left1)) == '\n') && ((temp_buf[temp_it] - (32 * i_right1)) != '\n')) ||
							((infile2_buf[temp_it2] - (32 * i_left1)) < (temp_buf[temp_it] - (32 * i_right1)))) {
							curr_vs_lastw = 1;
							break;
						}
						/* If the current rec is larger, set curr_vs_lastw = -1. */
						else if ((((infile2_buf[temp_it2] - (32 * i_left1)) != '\n') && ((temp_buf[temp_it] - (32 * i_right1)) == '\n')) ||
							((infile2_buf[temp_it2] - (32 * i_left1)) > (temp_buf[temp_it] - (32 * i_right1)))) {
							curr_vs_lastw = -1;
							break;
						} else {
							temp_it2++;
							temp_it++;
						}
					}
					/* If we reach here, it means the records are same */
					if (curr_vs_lastw == 0)
						printk("Last and current records are same");

				}
				should_we_write2 = 1;
				/* Flag checks: We know if our current rec is greater, lesser or equal to last written.
				 * Determine if we should write it based on flags set and space availablity in output buffer */
				if (curr_vs_lastw == 1) {
					if (kernel_args->flags & 0x10) {
						/* If test flag is set, delete the temp file and abort. We don't delete the output file if it existed */
						filp_close(outfile_ptr, NULL);
						filp_close(infile1_ptr, NULL);
						filp_close(infile2_ptr, NULL);
						retval = -EINVAL;
						goto clean_temp_buf;
					} else {
						/* Ignore the descending record, don't throw error since -t is not set */
						should_we_write2 = 0;
						while (infile2_buf[in2_p2] != '\n')
							in2_p2++;
						in2_p1 = ++in2_p2;
					}
				}
				/* If record is ascending, proceed to write it*/
				else if (curr_vs_lastw == -1)
					should_we_write2 = 1;
				/* If last written and current is same */
				else {
					/* If -a flag is set*/
					if (kernel_args->flags & 0x02)
						should_we_write2 = 1;
					else {
						/* Ignore the record and proceed */
						should_we_write2 = 0;
						while (infile2_buf[in2_p2] != '\n')
							in2_p2++;
						in2_p1 = ++in2_p2;
					}
				}
				if (should_we_write2) {
					if (len_curr_rec > rem_space_in_out) {
						/* Current rec does not fit in output buffer.
						 * Write the contents of output buffer to output file and clear it*/
						retval = write_file(outfile_ptr, outfile_buf, strlen(outfile_buf)-1);
						if (retval < 0) {
							printk(KERN_ALERT "File write from buffer failed!");
							retval = -EIO;
							goto clean_temp_buf;
						}
						memset(outfile_buf, 0x00, PAGE_SIZE);
					}
					rec_ctr++;
					temp_out = strlen(outfile_buf);
					temp_it = 0;
					memset(temp_buf, 0x00, PAGE_SIZE);
					/* Write the current record to output buffer as well as the temp buffer */
					while (len_curr_rec--) {
						outfile_buf[temp_out++] = infile2_buf[in2_p1];
						temp_buf[temp_it++] = infile2_buf[in2_p1];
						in2_p1++;
					}
					in2_p2 = in2_p1;
				}
			}
		}
		/* If the second file has ended but first hasn't, process the remaining
		 * records for the first file*/
		else if (chars_read2 == 0) {
			if (chars_read1 != 0) {
				len_curr_rec = 0;
				rem_space_in_out = PAGE_SIZE - strlen(outfile_buf);
				temp_it = in1_p1;
				/* Get length */
				while (infile1_buf[temp_it++] != '\n')
					len_curr_rec++;
				len_curr_rec++;
				curr_vs_lastw = -1;
				if (strlen(temp_buf) > 0) {
					/* Compare last written record with current */
					i_left1 = 0, i_right1 = 0, temp_it = 0;
					temp_it2 = in1_p1;
					curr_vs_lastw = 0;
					while (!((temp_buf[temp_it] == '\n') && (infile1_buf[temp_it2] == '\n'))) {
						if ((kernel_args->flags & 0x04) && (97 <= infile1_buf[temp_it2]) && (infile1_buf[temp_it2] <= 123))
							i_left1 = 1;
						if ((kernel_args->flags & 0x04) && (97 <= temp_buf[temp_it]) && (temp_buf[temp_it] <= 123))
							i_right1 = 1;
						if ((((infile1_buf[temp_it2] - (32 * i_left1)) == '\n') && ((temp_buf[temp_it] - (32 * i_right1)) != '\n')) ||
							((infile1_buf[temp_it2] - (32 * i_left1)) < (temp_buf[temp_it] - (32 * i_right1)))) {
							curr_vs_lastw = 1;
							break;
						} else if ((((infile1_buf[temp_it2] - (32 * i_left1)) != '\n') && ((temp_buf[temp_it] - (32 * i_right1)) == '\n')) ||
							((infile1_buf[temp_it2] - (32 * i_left1)) > (temp_buf[temp_it] - (32 * i_right1)))) {
							curr_vs_lastw = -1;
							break;
						} else {
							temp_it2++;
							temp_it++;
						}
					}
					if (curr_vs_lastw == 0)
						printk("Last and current records are same");
				}
				should_we_write1 = 1;
				/* If current record is smaller than previous written*/
				if (curr_vs_lastw == 1) {
					if (kernel_args->flags & 0x10) {
						filp_close(outfile_ptr, NULL);
						filp_close(infile1_ptr, NULL);
						filp_close(infile2_ptr, NULL);
						retval = -EINVAL;
						goto clean_temp_buf;
					} else {
						should_we_write1 = 0;
						while (infile1_buf[in1_p2] != '\n')
							in1_p2++;
						in1_p1 = ++in1_p2;
					}
				} else if (curr_vs_lastw == -1)
					should_we_write1 = 1;
				else {
					/* If -a flag is set */
					if (kernel_args->flags & 0x02)
						should_we_write1 = 1;
					else {
						should_we_write1 = 0;
						while (infile1_buf[in1_p2] != '\n')
							in1_p2++;
						in1_p1 = ++in1_p2;
					}
				}
				if (should_we_write1) {
					/* If the record is lengthier than remaining space in out buffer, clear the buffer by writing to file */
					if (len_curr_rec > rem_space_in_out) {
						retval = write_file(outfile_ptr, outfile_buf, strlen(outfile_buf)-1);
						if (retval < 0) {
							printk(KERN_ALERT "File write from buffer failed!");
							retval = -EIO;
							goto clean_temp_buf;
						}
						memset(outfile_buf, 0x00, PAGE_SIZE);
					}
					temp_out = strlen(outfile_buf);
					temp_it = 0;
					printk("Write to buf");
					rec_ctr++;
					memset(temp_buf, 0x00, PAGE_SIZE);
					/* Write the contents to buffer */
					while (len_curr_rec--) {
						outfile_buf[temp_out++] = infile1_buf[in1_p1];
						temp_buf[temp_it++] = infile1_buf[in1_p1];
						in1_p1++;
					}
					in1_p2 = in1_p1;
				}
			}
		}
		/* Both the input files are not done, perform comparison of records */
		else {
			i_left = 0, i_right = 0;
			/* Case-insensitivity flag*/
			if ((kernel_args->flags & 0x04) && (97 <= infile1_buf[in1_p2]) && (infile1_buf[in1_p2] <= 123))
				i_left = 1;
			if ((kernel_args->flags & 0x04) && (97 <= infile2_buf[in2_p2]) && (infile2_buf[in2_p2] <= 123))
				i_right = 1;
			/* If both the records in file 1 and file 2 are same */
			if (((infile2_buf[in2_p2] - (32 * i_left)) == '\n') && ((infile1_buf[in1_p2] - (32 * i_right)) == '\n')) {
				len_curr_rec = 0;
				rem_space_in_out = PAGE_SIZE - strlen(outfile_buf);
				temp_it = in1_p1;
				while (infile1_buf[temp_it++] != '\n')
					len_curr_rec++;
				len_curr_rec++;
				curr_vs_lastw = -1;
				if (strlen(temp_buf) > 0) {
					i_left1 = 0, i_right1 = 0, temp_it = 0;
					temp_it2 = in1_p1;
					curr_vs_lastw = 0;
					/* Since both the records in file 1 and file 2 are same, we compare only first one with last written */
					while (!((temp_buf[temp_it] == '\n') && (infile1_buf[temp_it2] == '\n'))) {
						if ((kernel_args->flags & 0x04) && (97 <= infile1_buf[temp_it2]) && (infile1_buf[temp_it2] <= 123))
							i_left1 = 1;
						if ((kernel_args->flags & 0x04) && (97 <= temp_buf[temp_it]) && (temp_buf[temp_it] <= 123))
							i_right1 = 1;
						if ((((infile1_buf[temp_it2] - (32 * i_left1)) == '\n') && ((temp_buf[temp_it] - (32 * i_right1)) != '\n')) ||
							((infile1_buf[temp_it2] - (32 * i_left1)) < (temp_buf[temp_it] - (32 * i_right1)))) {
							curr_vs_lastw = 1;
							break;
						} else if ((((infile1_buf[temp_it2] - (32 * i_left1)) != '\n') && ((temp_buf[temp_it] - (32 * i_right1)) == '\n')) ||
							((infile1_buf[temp_it2] - (32 * i_left1)) > (temp_buf[temp_it] - (32 * i_right1)))) {
							curr_vs_lastw = -1;
							break;
						} else {
							temp_it2++;
							temp_it++;
						}
					}
					if (curr_vs_lastw == 0)
						printk("Last and current records are same");
				}
				printk("rem_space_out: %d\n", rem_space_in_out);
				should_we_write1 = 1, should_we_write2 = 1;
				if (curr_vs_lastw == 1) {
					/* If a descending record is found */
					if (kernel_args->flags & 0x10) {
						filp_close(outfile_ptr, NULL);
						filp_close(infile1_ptr, NULL);
						filp_close(infile2_ptr, NULL);
						retval = -EINVAL;
						goto clean_temp_buf;
					} else {
						/* skip both the records since they are descending */
						should_we_write1 = 0;
						should_we_write2 = 0;
						while (infile1_buf[in1_p2] != '\n')
							in1_p2++;
						in1_p1 = ++in1_p2;
						while (infile2_buf[in2_p2] != '\n')
							in2_p2++;
						in2_p1 = ++in2_p2;
					}
				} else if (curr_vs_lastw == -1) {
					/* Write both the records if -a is set*/
					if (kernel_args->flags & 0x02) {
						should_we_write1 = 1;
						should_we_write2 = 1;
					} else {
					/* Write ONLY the first record if -u is set */
						should_we_write1 = 1;
						should_we_write2 = 0;
						in2_p2++;
						in2_p1 = in2_p2;
					}
				} else {
					/* Write both if -a is set */
					if (kernel_args->flags & 0x02) {
						should_we_write1 = 1;
						should_we_write2 = 1;
					} else {
					/* skip both the records since -u is set */
						should_we_write1 = 0;
						should_we_write2 = 0;
						while (infile1_buf[in1_p2] != '\n')
							in1_p2++;
						in1_p1 = ++in1_p2;
						while (infile2_buf[in2_p2] != '\n')
							in2_p2++;
						in2_p1 = ++in2_p2;
					}
				}
				if (should_we_write1) {
					/* Write the buffer to file since space is not available */
					if (len_curr_rec > rem_space_in_out) {
						retval = write_file(outfile_ptr, outfile_buf, strlen(outfile_buf)-1);
						if (retval < 0) {
							printk(KERN_ALERT "File write from buffer failed!");
							retval = -EIO;
							goto clean_temp_buf;
						}
						memset(outfile_buf, 0x00, PAGE_SIZE);
					}
					rec_ctr++;
					temp_out = strlen(outfile_buf);
					len_curr_rec2 = len_curr_rec;
					temp_it = 0;
					memset(temp_buf, 0x00, PAGE_SIZE);
					/* Write the record to buffer */
					while (len_curr_rec--) {
						outfile_buf[temp_out++] = infile1_buf[in1_p1];
						temp_buf[temp_it++] = infile1_buf[in1_p1];
						in1_p1++;
					}
					in1_p2 = in1_p1;
				}

				if (should_we_write2) {
					/* Write the second record after clearing the buffer */
					rem_space_in_out = PAGE_SIZE - strlen(outfile_buf);
					if (len_curr_rec2 > rem_space_in_out) {
						retval = write_file(outfile_ptr, outfile_buf, strlen(outfile_buf)-1);
						if (retval < 0) {
							printk(KERN_ALERT "File write from buffer failed!");
							retval = -EIO;
							goto clean_outfile_buf;
						}
						memset(outfile_buf, 0x00, PAGE_SIZE);
					}
					rec_ctr++;
					temp_out = strlen(outfile_buf);
					temp_it = 0;
					memset(temp_buf, 0x00, PAGE_SIZE);
					/* Write to buffer*/
					while (len_curr_rec2--) {
						outfile_buf[temp_out++] = infile2_buf[in2_p1];
						temp_buf[temp_it++] = infile2_buf[in2_p1];
						in2_p1++;
					}
					in2_p2 = in2_p1;
				}
			} else if ((((infile1_buf[in1_p2] - (32 * i_left)) == '\n') && ((infile2_buf[in2_p2] - (32 * i_right)) != '\n')) ||
				((infile1_buf[in1_p2] - (32 * i_left)) < (infile2_buf[in2_p2] - (32 * i_right)))) {
				/* If record from file1 is smaller than the one is file2 */
				len_curr_rec = 0;
				rem_space_in_out = PAGE_SIZE - strlen(outfile_buf);
				temp_it = in1_p1;
				/* Calculate length of record */
				while (infile1_buf[temp_it++] != '\n')
					len_curr_rec++;
				len_curr_rec++;
				curr_vs_lastw = -1;
				/* If temp buffer is not empty, */
				if (strlen(temp_buf) > 0) {
					i_left1 = 0, i_right1 = 0, temp_it = 0;
					temp_it2 = in1_p1;
					curr_vs_lastw = 0;
					while (!((temp_buf[temp_it] == '\n') && (infile1_buf[temp_it2] == '\n'))) {
						if ((kernel_args->flags & 0x04) && (97 <= infile1_buf[temp_it2]) && (infile1_buf[temp_it2] <= 123))
							i_left1 = 1;
						if ((kernel_args->flags & 0x04) && (97 <= temp_buf[temp_it]) && (temp_buf[temp_it] <= 123))
							i_right1 = 1;
						if ((((infile1_buf[temp_it2] - (32 * i_left1)) == '\n') && ((temp_buf[temp_it] - (32 * i_right1)) != '\n')) ||
							((infile1_buf[temp_it2] - (32 * i_left1)) < (temp_buf[temp_it] - (32 * i_right1)))) {
							curr_vs_lastw = 1;
							break;
						} else if ((((infile1_buf[temp_it2] - (32 * i_left1)) != '\n') && ((temp_buf[temp_it] - (32 * i_right1)) == '\n')) ||
							((infile1_buf[temp_it2] - (32 * i_left1)) > (temp_buf[temp_it] - (32 * i_right1)))) {
							curr_vs_lastw = -1;
							break;
						} else {
							temp_it2++;
							temp_it++;
						}
					}
					if (curr_vs_lastw == 0)
						printk("Last and current records are same");
				}
				should_we_write1 = 1;
				/*If the record is smaller than last written*/
				if (curr_vs_lastw == 1) {
					if (kernel_args->flags & 0x10) {
						filp_close(outfile_ptr, NULL);
						filp_close(infile1_ptr, NULL);
						filp_close(infile2_ptr, NULL);
						retval = -EINVAL;
						goto clean_temp_buf;
					} else {
						should_we_write1 = 0;
						while (infile1_buf[in1_p2] != '\n')
							in1_p2++;
						in1_p1 = ++in1_p2;
					}
				} else if (curr_vs_lastw == -1)
					/* If the record is lengthier than last written */
					should_we_write1 = 1; else {
					/* If the record is same as last written */
					if (kernel_args->flags & 0x02)
						should_we_write1 = 1; else {
						should_we_write1 = 0;
						while (infile1_buf[in1_p2] != '\n')
							in1_p2++;
						in1_p1 = ++in1_p2;
					}
				}
				if (should_we_write1) {
					/*  If the record is lengthier than remaining space in output buffer */
					if (len_curr_rec > rem_space_in_out) {
						retval = write_file(outfile_ptr, outfile_buf, strlen(outfile_buf)-1);
						if (retval < 0) {
							printk(KERN_ALERT "File write from buffer failed!");
							retval = -EIO;
							goto clean_temp_buf;
						}
						memset(outfile_buf, 0x00, PAGE_SIZE);
					}
					rec_ctr++;
					temp_out = strlen(outfile_buf);
					temp_it = 0;
					memset(temp_buf, 0x00, PAGE_SIZE);
					/* Write to output buffer */
					while (len_curr_rec--) {
						outfile_buf[temp_out++] = infile1_buf[in1_p1];
						temp_buf[temp_it++] = infile1_buf[in1_p1];
						in1_p1++;
					}
					in1_p2 = in1_p1;
				}
				in2_p2 = in2_p1;
			}
			/* If record 2 is smaller than record 1 */
			else if ((((infile2_buf[in2_p2] - (32 * i_left)) == '\n') && ((infile1_buf[in1_p2] - (32 * i_right)) != '\n')) ||
				((infile1_buf[in1_p2] - (32 * i_left)) > (infile2_buf[in2_p2] - (32 * i_right)))) {
				len_curr_rec = 0;
				rem_space_in_out = PAGE_SIZE - strlen(outfile_buf);
				temp_it = in2_p1;
				/* Get the length of current record */
				while (infile2_buf[temp_it++] != '\n')
					len_curr_rec++;
				len_curr_rec++;
				curr_vs_lastw = -1;
				if (strlen(temp_buf) > 0) {
					/* Compare current record with the last written record */
					curr_vs_lastw = 0;
					i_left1 = 0, i_right1 = 0, temp_it = 0;
					temp_it2 = in2_p1;
					while (!((temp_buf[temp_it] == '\n') && (infile2_buf[temp_it2] == '\n'))) {
						if ((kernel_args->flags & 0x04) && (97 <= infile2_buf[temp_it2]) && (infile2_buf[temp_it2] <= 123))
							i_left1 = 1;
						if ((kernel_args->flags & 0x04) && (97 <= temp_buf[temp_it]) && (temp_buf[temp_it] <= 123))
							i_right1 = 1;
						if ((((infile2_buf[temp_it2] - (32 * i_left1)) == '\n') && ((temp_buf[temp_it] - (32 * i_right1)) != '\n')) ||
							((infile2_buf[temp_it2] - (32 * i_left1)) < (temp_buf[temp_it] - (32 * i_right1)))) {
							curr_vs_lastw = 1;
							break;
						} else if ((((infile2_buf[temp_it2] - (32 * i_left1)) != '\n') && ((temp_buf[temp_it] - (32 * i_right1)) == '\n')) ||
							((infile2_buf[temp_it2] - (32 * i_left1)) > (temp_buf[temp_it] - (32 * i_right1)))) {
							curr_vs_lastw = -1;
							break;
						} else {
							temp_it2++;
							temp_it++;
						}
					}
					if (curr_vs_lastw == 0)
						printk("Last and current records are same");

				}
				should_we_write2 = 1;
				/* If a decreasing record is found */
				if (curr_vs_lastw == 1) {
					if (kernel_args->flags & 0x10) {
						filp_close(outfile_ptr, NULL);
						filp_close(infile1_ptr, NULL);
						filp_close(infile2_ptr, NULL);
						retval = -EINVAL;
						goto clean_temp_buf;
					} else {
						should_we_write2 = 0;
						while (infile2_buf[in2_p2] != '\n')
							in2_p2++;
						in2_p1 = ++in2_p2;
					}
				} else if (curr_vs_lastw == -1)
					should_we_write2 = 1; else {
					if (kernel_args->flags & 0x02)
						should_we_write2 = 1; else {
						should_we_write2 = 0;
						while (infile2_buf[in2_p2] != '\n')
							in2_p2++;
						in2_p1 = ++in2_p2;
					}
				}
				if (should_we_write2) {
					/* If the record is lengthier than remaining space in buffer*/
					if (len_curr_rec > rem_space_in_out) {
						retval = write_file(outfile_ptr, outfile_buf, strlen(outfile_buf)-1);
						if (retval < 0) {
							printk(KERN_ALERT "File write from buffer failed!");
							retval = -EIO;
							goto clean_temp_buf;
						}
						memset(outfile_buf, 0x00, PAGE_SIZE);
					}
					rec_ctr++;
					temp_out = strlen(outfile_buf);
					temp_it = 0;
					memset(temp_buf, 0x00, PAGE_SIZE);
					/* Write to buffer */
					while (len_curr_rec--) {
						outfile_buf[temp_out++] = infile2_buf[in2_p1];
						temp_buf[temp_it++] = infile2_buf[in2_p1];
						in2_p1++;
					}
					in2_p2 = in2_p1;
				}
				in1_p2 = in1_p1;
			} else {
				in1_p2++;
				in2_p2++;
			}
		}
	}
	/* If there are records to write in output buffer, process */
	strncat(outfile_buf, "\n\0", 2);
	if (strlen(outfile_buf) > 1)
		retval = write_file(outfile_ptr, outfile_buf, strlen(outfile_buf)-1);
	if (retval < 0) {
		printk(KERN_ALERT "File write from buffer failed!");
		goto clean_outfile_buf;
	}
	ptr = &rec_ctr;
	/* Write the total number of records to the user specified data buffer, if -d is set */
	if (kernel_args->flags & 0x20)
		copy_to_user((void *) user_args->data, (void *) ptr, sizeof(unsigned int));


	/* Clean up all the file pointers */
	filp_close(outfile_ptr, NULL);
	filp_close(infile1_ptr, NULL);
	filp_close(infile2_ptr, NULL);
	retval = 0;

clean_temp_buf:
	kfree(temp_buf);
	temp_buf = NULL;
clean_outfile_buf:
	kfree(outfile_buf);
	outfile_buf = NULL;
clean_infile2_buf:
	kfree(infile2_buf);
	infile2_buf = NULL;
clean_infile1_buf:
	kfree(infile1_buf);
	infile1_buf = NULL;
	return retval;
}

int merge_util(xmergesort_args *kernel_args, xmergesort_args *user_args)
{
	int retval = 0, in_iter = 0, op_cr = 1;
	char *temp_outfile_prev = NULL;
	char  *temp_outfile_curr = NULL;
	struct file *outfile_ptr = NULL;
	struct file *tempfile_ptr = NULL;
	struct dentry *dentry_outfile = NULL;
	struct dentry *dentry_tempfile = NULL;
	struct kstat outfile_stat;
	struct kstat min_infile_stat;
	umode_t outfile_mode;
	umode_t infile_mode;
	mm_segment_t old_fs;
	/* If output file already exists, do not change permission.
	*  If a new file needs to be created, use the permission mode of most restricted input file.
	*/
	if (0 == vfs_stat(user_args->outfile, &outfile_stat)) {
		outfile_mode = outfile_stat.mode;
		op_cr = 0;
	} else {
		vfs_stat(user_args->infiles[0], &min_infile_stat);
		infile_mode = min_infile_stat.mode;
		for (in_iter = 0; in_iter < kernel_args->num_input_files; in_iter++) {
			if (0 == vfs_stat(user_args->infiles[in_iter], &min_infile_stat)) {
				if (infile_mode > min_infile_stat.mode)
					infile_mode = min_infile_stat.mode;
			}
		}
		outfile_mode = infile_mode;
	}

	outfile_ptr = open_outfile(kernel_args->outfile, outfile_mode);
	dentry_outfile = outfile_ptr->f_path.dentry;

	for (in_iter = 0; in_iter < kernel_args->num_input_files-1; in_iter++) {
		if (in_iter == 0) {
			temp_outfile_prev = kmalloc(strlen(kernel_args->outfile) + 10, GFP_KERNEL);
			snprintf(temp_outfile_prev, strlen(kernel_args->outfile) + 10, "%s.t.%d", kernel_args->outfile, in_iter);
			retval = merge_sort(kernel_args, user_args, kernel_args->infiles[in_iter], kernel_args->infiles[in_iter+1], temp_outfile_prev, outfile_mode);
		} else {
			temp_outfile_curr = kmalloc(strlen(kernel_args->outfile) + 10, GFP_KERNEL);
			snprintf(temp_outfile_curr, strlen(kernel_args->outfile) + 10, "%s.t.%d", kernel_args->outfile, in_iter);
			retval = merge_sort(kernel_args, user_args, temp_outfile_prev, kernel_args->infiles[in_iter+1], temp_outfile_curr, outfile_mode);
			temp_outfile_prev = temp_outfile_curr;
		}
		if (retval)
			goto error_cleanup;
	}

	/* Merge was successful, rename temp file to output file */
	tempfile_ptr = open_outfile(temp_outfile_prev, outfile_mode);
	dentry_tempfile = tempfile_ptr->f_path.dentry;
	lock_rename(dentry_tempfile->d_parent, dentry_outfile->d_parent);
	old_fs = get_fs();
	set_fs(KERNEL_DS);
	vfs_rename(dentry_tempfile->d_parent->d_inode, dentry_tempfile, dentry_outfile->d_parent->d_inode, dentry_outfile, NULL, 0);
	set_fs(old_fs);
	unlock_rename(dentry_tempfile->d_parent, dentry_outfile->d_parent);
	goto out;

error_cleanup:
	if ((retval == -22) && op_cr) {
		/* If we created an output file, delete that  */
		old_fs = get_fs();
		set_fs(KERNEL_DS);
		vfs_unlink(dentry_outfile->d_parent->d_inode, dentry_outfile, NULL);
		set_fs(old_fs);
	}
out:
	for (in_iter = 0; in_iter < kernel_args->num_input_files; in_iter++) {
		snprintf(temp_outfile_prev, strlen(kernel_args->outfile) + 10, "%s.t.%d", kernel_args->outfile, in_iter);
		tempfile_ptr = open_outfile(temp_outfile_prev, outfile_mode);
		if (NULL != tempfile_ptr) {
			dentry_tempfile = tempfile_ptr->f_path.dentry;
			old_fs = get_fs();
			set_fs(KERNEL_DS);
			vfs_unlink(dentry_tempfile->d_parent->d_inode, dentry_tempfile, NULL);
			set_fs(old_fs);
		}
	}
	return retval;
}


asmlinkage long xmergesort(void *arg)
{
	int ret_val = 0;
	xmergesort_args *kernel_args = NULL;
	xmergesort_args *user_args = (xmergesort_args *) arg;
	printk(KERN_ALERT "Entering system call() : xmergesort");
	kernel_args = kmalloc(sizeof(xmergesort_args), GFP_KERNEL);
	/* Check if user args pointer is accessible */
	if (NULL != user_args) {
		if (!access_ok(VERIFY_READ, user_args, sizeof(user_args))) {
			printk(KERN_ALERT "INVALID pointer to user space arg struct detected!");
			ret_val = -EFAULT;
			goto xmergesort_return;
		}
	} else {
		printk(KERN_ALERT "User space struct pointer detected NULL!");
		ret_val = -EINVAL;
		goto xmergesort_return;
	}
	/* Check if kernel struct got allocated memory */
	if (NULL == kernel_args) {
		printk(KERN_ALERT "Unable to allocate memory to kernel arg struct. Abort!");
		ret_val = -ENOMEM;
		goto xmergesort_return;
	}
	/* Validate all user args parameters and copy them to kernel struct */
	ret_val = init_params(kernel_args, user_args);
	if (0 != ret_val) {
		printk(KERN_ALERT "Kernel space parameters initialization failed. Abort!");
		goto xmergesort_return;
	}
	/* Proceed to merge sort routine if kernel args copy was successful */
	ret_val = merge_util(kernel_args, user_args);
	if (0 != ret_val)
		printk(KERN_ALERT "Merge operation failed"); else
		printk(KERN_ALERT "Merge operation was successful");

xmergesort_return:
	kfree(kernel_args);
	printk(KERN_ALERT "Exiting sys call() : xmergesort");
	return ret_val;
}
static int __init init_sys_xmergesort(void)
{
	printk("installed new sys_mergesort module\n");
	if (sysptr == NULL)
		sysptr = xmergesort;
	return 0;
}
static void  __exit exit_sys_xmergesort(void)
{
	if (sysptr != NULL)
		sysptr = NULL;
	printk("removed sys_xmergesort module\n");
}
module_init(init_sys_xmergesort);
module_exit(exit_sys_xmergesort);
MODULE_LICENSE("GPL");
